//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by C:\Users\Nikita\Desktop\FFFinder\rc\resource.rc
//
#define IDD_MAIN                        103
#define IDD_SEARCH_RESULT               105
#define IDB_LIST_BACKGROUND             107
#define IDB_BACK                        110
#define IDB_SEARCH_DIALOG_SKIN          112
#define IDB_CANCEL                      113
#define IDB_SEARCH                      114
#define IDB_BACK_HOVER                  115
#define IDB_CANCEL_HOVER                117
#define IDB_SEARCH_HOVER                118
#define IDB_BACK_PRESSED                119
#define IDB_CANCEL_PRESSED              120
#define IDB_SEARCH_PRESSED              121
#define IDC_EDIT_EXPRESSION             1003
#define IDC_BUTTON_SEARCH               1004
#define IDC_LIST_SEARCH_RESULT          1006
#define IDC_BUTTON_CANCEL               1007
#define IDC_BUTTON_BACK                 1008
#define IDC_STATIC_SEARCH_RESULT        1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        122
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
