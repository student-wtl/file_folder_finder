#include "abstract_thread.h"


DWORD FFFinder::AbstractThread::threadProc( LPVOID param )
{
    AbstractThread* pThis = reinterpret_cast<AbstractThread*>(param);
    return pThis->run();
}

FFFinder::AbstractThread::~AbstractThread()
{
    stop();
}

void FFFinder::AbstractThread::start()
{
    stop();
    hTerminateEvent = CreateEvent( 0, TRUE, FALSE, 0 );
    m_hThread = CreateThread( NULL, 0, threadProc, reinterpret_cast<LPVOID>(this), 0, &m_idThread );
}

void FFFinder::AbstractThread::stop()
{
    if( m_hThread )
    {
        if( WaitForSingleObject( m_hThread, 0 ) != WAIT_OBJECT_0 )
        {
            SetEvent( hTerminateEvent );
            WaitForSingleObject( m_hThread, INFINITE );
        }

        CloseHandle( m_hThread );
        CloseHandle( hTerminateEvent );
        m_hThread = NULL;
        hTerminateEvent = NULL;
    }
}
