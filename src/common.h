#pragma once
#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlddx.h>
#include <atlframe.h>
#include <atlstr.h>
#include <atlctrls.h>