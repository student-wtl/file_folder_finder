#pragma once
#include "abstract_thread.h"

#include <string>

#include "common.h"

namespace FFFinder
{
    class ThreadFileFolderFinder : public AbstractThread
    {
    public:
        ThreadFileFolderFinder( CListViewCtrl& lvTableSearchResult, const std::string& strDirectoryRoot );
        ~ThreadFileFolderFinder();

        void setSearchExpression( const std::string& strSearchExpression );

    private:
        CListViewCtrl& m_lvSearchResult;
        std::string m_strDirectoryRoot;
        std::string m_strSearchExpression;
        HANDLE m_hMutex;
        int m_nResultCounter = 0;

        virtual DWORD run() override;

        void addResult( const WIN32_FIND_DATA& resultData, const std::string& strFolder, const std::string& type );
        void findFile( const std::string& strDirectoryCurrent );
        void initItem( const std::string& strName,
                       const std::string& strSize,
                       const std::string& strType,
                       const std::string& strDate,
                       const std::string& strFolder );
    };
}

