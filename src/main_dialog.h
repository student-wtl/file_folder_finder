#pragma once
#include "common.h"
#include "resource.h"

#include <string>

#include "search_result_dialog.h"
#include "styled_button.h"


namespace FFFinder
{
    class MainDialog :
        public CDialogImpl<MainDialog>,
        public CWinDataExchange<MainDialog>
    {
    public:
        MainDialog();

        enum 
        {   
            IDD = IDD_MAIN
        };
        
        BEGIN_MSG_MAP( MainDialog )
            MESSAGE_HANDLER( WM_INITDIALOG, onInitDialog )
            MESSAGE_HANDLER( WM_CLOSE, onClose )
            MESSAGE_HANDLER( WM_ERASEBKGND, onEraseBkgnd )
            COMMAND_HANDLER( IDC_BUTTON_SEARCH, BN_CLICKED, onButtonSearchClicked )
            REFLECT_NOTIFICATIONS()
        END_MSG_MAP()

        BEGIN_DDX_MAP( MainDialog )
            DDX_TEXT_LEN( IDC_EDIT_EXPRESSION, m_strSearchExpression, MAX_PATH )
        END_DDX_MAP()
       
    private:
        CEdit m_editSearchExpression;
        StyledButton m_butSearch;
        CHAR m_strSearchExpression[MAX_PATH + 1] = {};
        std::string m_strDirectoryRoot;

        LRESULT onInitDialog( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onClose( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onButtonSearchClicked( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled ); 
        LRESULT onEraseBkgnd( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
    };
}
