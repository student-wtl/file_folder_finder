
#include "main_dialog.h"

CAppModule _Module;

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE, LPSTR, int )
{
    
    int res = 0;
    _Module.Init( 0, hInstance, 0 );

    FFFinder::MainDialog dialog;
    res = dialog.DoModal();

    _Module.Term();
    return res;
}