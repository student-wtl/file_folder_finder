#include "styled_list_view.h"
#include "smart_drawer.h"


FFFinder::StyledListView::StyledListView() :
    m_hBrushHeaderBk( CreateSolidBrush( RGB( 210, 233, 233 ) ) ),
    m_hBrushSelectedRowBk( CreateSolidBrush( RGB( 229, 240, 241 ) ) ),
    m_hBitmapBackground( LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_LIST_BACKGROUND ) ) ),
    m_colorSelectedRowBorder( RGB( 203, 206, 206 ) ),
    m_colorHeaderBorder( RGB( 175, 173, 173 ) )
{
}

FFFinder::StyledListView::~StyledListView()
{
    DeleteObject( m_hBrushHeaderBk );
    DeleteObject( m_hBrushSelectedRowBk );
    DeleteObject( m_hBitmapBackground );
}

DWORD FFFinder::StyledListView::OnPrePaint( int idCtrl, LPNMCUSTOMDRAW lpNMCD )
{
    return CDRF_NOTIFYITEMDRAW;
}

DWORD FFFinder::StyledListView::OnItemPrePaint( int idCtrl, LPNMCUSTOMDRAW lpNMCD )
{
    if( idCtrl == 0 )
    {
        drawHeaderItem( lpNMCD->hdc, &lpNMCD->rc, lpNMCD->dwItemSpec );
        return CDRF_SKIPDEFAULT;
    }

    if( lpNMCD->uItemState & CDIS_SELECTED )
    {
        lpNMCD->uItemState = CDIS_DEFAULT;
        RECT rectItem = {};
        GetItemRect( lpNMCD->dwItemSpec, &rectItem, LVIR_BOUNDS );
        rectItem.left += 2;
        rectItem.right -= 2;

        HANDLE hPenOld = SelectObject( lpNMCD->hdc, CreatePen( PS_SOLID, 1, m_colorSelectedRowBorder ) );
        HANDLE hBrushOld = SelectObject( lpNMCD->hdc, m_hBrushSelectedRowBk );
        ::FillRect( lpNMCD->hdc, &rectItem, m_hBrushSelectedRowBk );
        Rectangle( lpNMCD->hdc, rectItem.left, rectItem.top, rectItem.right, rectItem.bottom );
        DeleteObject( SelectObject( lpNMCD->hdc, hPenOld ) );
        SelectObject( lpNMCD->hdc, hBrushOld );
    }
    return CDRF_NOTIFYITEMDRAW;
}

DWORD FFFinder::StyledListView::OnSubItemPrePaint( int idCtrl, LPNMCUSTOMDRAW lpNMCD )
{
    NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>(lpNMCD);

    CString buffer;
    GetItemText( pLVCD->nmcd.dwItemSpec, pLVCD->iSubItem, buffer );

    RECT rectSubItem = {};
    GetSubItemRect( pLVCD->nmcd.dwItemSpec, pLVCD->iSubItem, LVIR_BOUNDS, &rectSubItem );
    if( pLVCD->iSubItem == 0 )
    {
        int widthFisrtsColumn = GetColumnWidth( 0 );
        rectSubItem.right = widthFisrtsColumn + rectSubItem.left;
    }
    drawSubItem( lpNMCD->hdc, buffer, &rectSubItem, pLVCD->nmcd.dwItemSpec, pLVCD->iSubItem, lpNMCD->uItemState );
    return CDRF_SKIPDEFAULT;
}

LRESULT FFFinder::StyledListView::onEraseBkgnd( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    HDC hDC = reinterpret_cast<HDC>(wParam);
    RECT rect = {};
    GetClientRect( &rect );

    HDC hLocalDC = CreateCompatibleDC( hDC );
    BITMAP bitmap;
    GetObject( m_hBitmapBackground, sizeof( BITMAP ), reinterpret_cast<LPSTR>(&bitmap) );
    HBITMAP hOldBmp = (HBITMAP)::SelectObject( hLocalDC, m_hBitmapBackground );

    StretchBlt( hDC,
                rect.left,
                rect.top,
                rect.right,
                rect.bottom,
                hLocalDC,
                0,
                0,
                bitmap.bmWidth,
                bitmap.bmHeight,
                SRCCOPY );

    SelectObject( hLocalDC, hOldBmp );
    DeleteDC( hLocalDC );
    return true;
}

LRESULT FFFinder::StyledListView::onMouseWheel( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    Invalidate();
    bHandled = false;
    return LRESULT();
}

LRESULT FFFinder::StyledListView::onHScroll( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{

    Invalidate();
    bHandled = false;
    return LRESULT();
}

LRESULT FFFinder::StyledListView::onVScroll( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    Invalidate();
    bHandled = false;
    return LRESULT();
}

LRESULT FFFinder::StyledListView::onLButtonDown( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    Invalidate();
    bHandled = false;
    return LRESULT();
}

LRESULT FFFinder::StyledListView::onColumnResized( int idCtrl, LPNMHDR pnmh, BOOL& bHandled )
{
    Invalidate();
    return LRESULT();
}

void FFFinder::StyledListView::drawHeaderItem( HDC hDC, LPRECT lpRect, int index )
{
    CHAR szName[20] = {};
    LVCOLUMN lvColInfo;
    lvColInfo.mask = LVCF_TEXT;
    lvColInfo.pszText = szName;
    lvColInfo.cchTextMax = sizeof( szName );
    GetColumn( index, &lvColInfo );

    ::FillRect( hDC, lpRect, m_hBrushHeaderBk );

    HANDLE hPenOld = SelectObject( hDC, CreatePen( PS_SOLID, 0, m_colorHeaderBorder ) );
    HANDLE hBrushOld = SelectObject( hDC, GetStockObject( NULL_BRUSH ) );

    Rectangle( hDC, lpRect->left, lpRect->top, lpRect->right, lpRect->bottom );
    SetBkMode( hDC, TRANSPARENT );
    DrawText( hDC,
              lvColInfo.pszText,
              strlen( szName ),
              lpRect,
              DT_VCENTER | DT_SINGLELINE | DT_CENTER | DT_WORD_ELLIPSIS );
    SelectObject( hDC, hBrushOld );
    DeleteObject( SelectObject( hDC, hPenOld ) );
}

void FFFinder::StyledListView::drawSubItem( HDC hDC, CString strText, LPRECT lpRect, int iItem, int iSubItem, UINT uItemState )
{
    int offset = 6;
    lpRect->left += offset;
    lpRect->right -= offset;
    SetBkMode( hDC, TRANSPARENT );
    DrawText( hDC, strText, strText.GetLength(), lpRect, DT_VCENTER | DT_SINGLELINE | DT_WORD_ELLIPSIS | DT_LEFT );
}
