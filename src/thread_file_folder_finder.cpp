#include "thread_file_folder_finder.h"
#include <list>

FFFinder::ThreadFileFolderFinder::ThreadFileFolderFinder( CListViewCtrl& lvTableSearchResult, const std::string& strDirectoryRoot ) :
    m_lvSearchResult(lvTableSearchResult),
    m_strDirectoryRoot( strDirectoryRoot )
{
    m_hMutex = CreateMutex( NULL, FALSE, NULL );             
}

FFFinder::ThreadFileFolderFinder::~ThreadFileFolderFinder()
{
    CloseHandle( m_hMutex );
}

void FFFinder::ThreadFileFolderFinder::setSearchExpression( const std::string& strSearchExpression )
{
    DWORD dwWaitResult = WaitForSingleObject( m_hMutex, INFINITE );
    if( dwWaitResult == WAIT_OBJECT_0 )
    {
        m_strSearchExpression = strSearchExpression;
        ReleaseMutex( m_hMutex );
    }
}

DWORD FFFinder::ThreadFileFolderFinder::run()
{
    if ( m_strDirectoryRoot.empty() || m_strSearchExpression.empty() )
        return -1;

    DWORD dwWaitResult = WaitForSingleObject( m_hMutex, INFINITE );
    if( dwWaitResult == WAIT_OBJECT_0 )
    {
        SetCursor( LoadCursor( NULL, IDC_WAIT ) );
        ShowCursor( true );

        findFile( m_strDirectoryRoot );

        SetCursor( LoadCursor( NULL, IDC_ARROW ) );
        ShowCursor( true );
        ReleaseMutex( m_hMutex );
    }
    return 0;
}

void FFFinder::ThreadFileFolderFinder::addResult( const WIN32_FIND_DATA& resultData, const std::string& strFolder, const std::string& type )
{
    LARGE_INTEGER iSizeFile = {};  
    iSizeFile.LowPart = resultData.nFileSizeLow;
    iSizeFile.HighPart = resultData.nFileSizeHigh;

    std::string bufTime( 17, 0 );
    SYSTEMTIME sysTime = {}; 
    if( !FileTimeToSystemTime( &resultData.ftCreationTime, &sysTime ) )
        return;
    sprintf_s( &bufTime[0],
               bufTime.size(),
               "%04d/%02d/%02d %02d:%02d",
               sysTime.wYear,
               sysTime.wMonth,
               sysTime.wDay,
               sysTime.wHour,
               sysTime.wMinute );
        
    initItem( resultData.cFileName, std::to_string( iSizeFile.QuadPart ), type, bufTime, strFolder );
}

void FFFinder::ThreadFileFolderFinder::findFile( const std::string& strDirectoryCurrent )
{
    WIN32_FIND_DATA findData = {};
    HANDLE hFile = FindFirstFile( (strDirectoryCurrent + "*").c_str(), &findData );
    if( hFile != INVALID_HANDLE_VALUE )
    {
        std::list<std::string> directories;
        do
        {
            if( findData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN )
                continue;
            if( findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
            {
                if( strcmp( findData.cFileName, "." ) == 0 || strcmp( findData.cFileName, ".." ) == 0 )
                    continue;
                if( std::string( findData.cFileName ).find( m_strSearchExpression ) != std::string::npos )
                    addResult( findData, strDirectoryCurrent + findData.cFileName + "\\", "directory" );
                directories.push_back( strDirectoryCurrent + findData.cFileName + "\\" );
            }
            else
                if( std::string( findData.cFileName ).find( m_strSearchExpression ) != std::string::npos )
                    addResult( findData, strDirectoryCurrent, "file" );
        } while( FindNextFile( hFile, &findData ) );
        FindClose( hFile );
        for( std::list<std::string>::iterator iter = directories.begin(), end = directories.end(); iter != end; ++iter )
            findFile( *iter );
    }
}

void FFFinder::ThreadFileFolderFinder::initItem( const std::string& strName,
                                                 const std::string& strSize,
                                                 const std::string& strType,
                                                 const std::string& strDate,
                                                 const std::string& strFolder )
{
    LVITEM lvItem = {};
    lvItem.iItem = m_nResultCounter++;
    if( m_lvSearchResult.InsertItem( &lvItem ) == -1 )
        return;

    lvItem.mask = LVIF_TEXT;
    auto itemSetter = [&lvItem, this]( int iSubItem, const std::string& text ) -> bool
    {
        lvItem.iSubItem = iSubItem;
        lvItem.pszText = const_cast<LPSTR>(&text[0]);
        if( !m_lvSearchResult.SetItem( &lvItem ) )
            return false;
        return true;
    };

    if( !itemSetter( 0, strName ) )
        return;
    if( !itemSetter( 1, strSize ) )
        return;
    if( !itemSetter( 2, strType ) )
        return;
    if( !itemSetter( 3, strDate ) )
        return;
    if( !itemSetter( 4, strFolder ) )
        return;
}

