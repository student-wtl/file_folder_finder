#pragma once

#include "common.h"

namespace FFFinder
{
    class StyledButton :
        public CWindowImpl<StyledButton, CButton>,
        public COwnerDraw<StyledButton>
    {
    public:
        StyledButton(HBITMAP hBitmapDefault, HBITMAP hBitmapPressed, HBITMAP hBitmapHovered );
        ~StyledButton();

        BEGIN_MSG_MAP( StyledButton )
            CHAIN_MSG_MAP_ALT( COwnerDraw<StyledButton>, 1 )
            MESSAGE_HANDLER( WM_MOUSELEAVE, onMouseLeave )
            MESSAGE_HANDLER( WM_MOUSEMOVE, onMouseMove )
        END_MSG_MAP()

        void DrawItem( LPDRAWITEMSTRUCT lpDIS );
    private:
        HBITMAP m_hBitmapDefault;
        HBITMAP m_hBitmapPressed;
        HBITMAP m_hBitmapHovered;

        bool m_isCursorHovered = false;

        LRESULT onMouseMove( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onMouseLeave( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
    };
}
