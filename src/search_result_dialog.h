#pragma once
#include <memory>
#include "thread_file_folder_finder.h"

#include "resource.h"
#include "styled_list_view.h"
#include "styled_button.h"

namespace FFFinder
{
    class SearchResultDialog :
        public CDialogImpl<SearchResultDialog>
    {
    public:
        enum
        {
            IDD = IDD_SEARCH_RESULT
        };

        SearchResultDialog( const std::string& strDirectoryRoot, const std::string& strSearchExpression );

        BEGIN_MSG_MAP( FFFinder::SearchResultDialog )
            MESSAGE_HANDLER( WM_INITDIALOG, onInitDialog )
            MESSAGE_HANDLER( WM_CLOSE, onClose )
            COMMAND_HANDLER( IDC_BUTTON_BACK, BN_CLICKED, onButtonBackClicked )
            COMMAND_HANDLER( IDC_BUTTON_CANCEL, BN_CLICKED, onButtonCancelClicked )
            MESSAGE_HANDLER( WM_ERASEBKGND, onEraseBkgnd )
            CHAIN_MSG_MAP_MEMBER( m_lvSearchResult )
            REFLECT_NOTIFICATIONS()
        END_MSG_MAP()

    private:
        StyledListView m_lvSearchResult;
        StyledButton m_butCancel;
        StyledButton m_butBack;
        ThreadFileFolderFinder m_fileFolderFinder;
        HBITMAP m_hBitmapSkin;

        void initColumns();

        LRESULT onInitDialog( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onClose( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onButtonBackClicked( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled );
        LRESULT onButtonCancelClicked( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled );
        LRESULT onEraseBkgnd( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
    };
}
