#pragma once
#include "common.h"
#include "resource.h"

namespace FFFinder
{
    class StyledListView :
        public CWindowImpl<StyledListView, CListViewCtrl>,
        public CCustomDraw<StyledListView>
    {
    public:
        StyledListView();
        ~StyledListView();

        BEGIN_MSG_MAP( StyledListView )
            CHAIN_MSG_MAP( CCustomDraw<StyledListView> )
            MESSAGE_HANDLER( WM_ERASEBKGND, onEraseBkgnd )
            MESSAGE_HANDLER( WM_HSCROLL, onHScroll )
            MESSAGE_HANDLER( WM_VSCROLL, onVScroll )
            MESSAGE_HANDLER( WM_MOUSEWHEEL, onMouseWheel )
            MESSAGE_HANDLER( WM_LBUTTONDOWN, onLButtonDown )
            NOTIFY_HANDLER( 0, HDN_ITEMCHANGED, onColumnResized )
            DEFAULT_REFLECTION_HANDLER()
        END_MSG_MAP()

        DWORD OnPrePaint( int idCtrl, LPNMCUSTOMDRAW lpNMCD );
        DWORD OnItemPrePaint( int idCtrl, LPNMCUSTOMDRAW lpNMCD );
        DWORD OnSubItemPrePaint( int idCtrl, LPNMCUSTOMDRAW lpNMCD );

    private:
        HBRUSH m_hBrushHeaderBk;
        HBRUSH m_hBrushSelectedRowBk;
        HBITMAP m_hBitmapBackground;
        COLORREF m_colorSelectedRowBorder;
        COLORREF m_colorHeaderBorder;

        LRESULT onEraseBkgnd( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onMouseWheel( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onHScroll( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onVScroll( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onLButtonDown( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );

        LRESULT onColumnResized( int idCtrl, LPNMHDR pnmh, BOOL& bHandled );

        void drawHeaderItem( HDC hDC, LPRECT lpRect, int index );
        void drawSubItem( HDC hDC, CString strText, LPRECT lpRect, int iItem, int iSubItem, UINT uItemState );
    };
}
