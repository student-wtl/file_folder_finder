#include "smart_drawer.h"

FFFinder::SmartDrawer::SmartDrawer( HDC hDc ) :
    m_hDc( hDc )
{
}

bool FFFinder::SmartDrawer::fillGradientRect( POINT pointLT,
                               POINT pointRB,
                               COLORREF colorStart, 
                               COLORREF colorEnd,
                               bool isVertical )
{
    TRIVERTEX vertex[2] = {};
    initVertext( vertex[0], pointLT, colorStart );
    initVertext( vertex[1], pointRB, colorEnd );
   
    GRADIENT_RECT gRect;
    gRect.UpperLeft = 0;
    gRect.LowerRight = 1;

    if( isVertical )
        return GradientFill( m_hDc, vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_V );
    else
        return GradientFill( m_hDc, vertex, 2, &gRect, 1, GRADIENT_FILL_RECT_H );

}

bool FFFinder::SmartDrawer::fillGradientTriangle( POINT point1,
                                   POINT point2,
                                   POINT point3,
                                   COLORREF color1,
                                   COLORREF color2,
                                   COLORREF color3 )
{
    TRIVERTEX vertex[3] = {};
    initVertext( vertex[0], point1, color1 );
    initVertext( vertex[1], point2, color2 );
    initVertext( vertex[2], point3, color3 );
   
    GRADIENT_TRIANGLE gTriangle;
    gTriangle.Vertex1 = 0;
    gTriangle.Vertex2 = 1;
    gTriangle.Vertex3 = 2;

    return GradientFill( m_hDc, vertex, 3, &gTriangle, 1, GRADIENT_FILL_TRIANGLE );
}

bool FFFinder::SmartDrawer::drawLine( int x1, int y1, int x2, int y2 )
{
    MoveToEx( m_hDc, x1, y1, NULL );
    return LineTo( m_hDc, x2, y2 );
}

HDC FFFinder::SmartDrawer::getHDC()
{
    return m_hDc;
}

void FFFinder::SmartDrawer::initVertext( TRIVERTEX& vertex, POINT point, COLORREF color )
{
    vertex.x = point.x;
    vertex.y = point.y;
    vertex.Red = GetRValue( color ) << 8;
    vertex.Green = GetGValue( color ) << 8;
    vertex.Blue = GetBValue( color ) << 8;
}
