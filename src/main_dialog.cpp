#include "main_dialog.h"
#include <Shlobj_core.h>
#include "smart_drawer.h"


FFFinder::MainDialog::MainDialog() : 
    CDialogImpl< MainDialog>(),
    m_butSearch( LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_SEARCH ) ),
                 LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_SEARCH_PRESSED ) ),
                 LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_SEARCH_HOVER ) ) )
{
}

LRESULT FFFinder::MainDialog::onInitDialog( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    m_editSearchExpression = GetDlgItem( IDC_EDIT_EXPRESSION );
    m_butSearch.SubclassWindow( GetDlgItem( IDC_BUTTON_SEARCH ) );

    m_strDirectoryRoot.resize( MAX_PATH, 0 );
    HRESULT hResult = SHGetFolderPath( NULL, CSIDL_MYDOCUMENTS, NULL, 0, &m_strDirectoryRoot[0] );

    if( hResult == S_OK )
    {
        size_t pos = m_strDirectoryRoot.find( '\0' );
        m_strDirectoryRoot.resize( pos + 1 );
        m_strDirectoryRoot[pos] = '\\';
        m_strDirectoryRoot.shrink_to_fit();
    }

    if( ::GetDlgCtrlID( reinterpret_cast<HWND>(wParam) ) != IDC_EDIT_EXPRESSION )
    {
        ::SetFocus( GetDlgItem( IDC_EDIT_EXPRESSION ) );
        return FALSE;
    }

    return TRUE;
}

LRESULT FFFinder::MainDialog::onClose( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    EndDialog( 0 );
    return LRESULT();
}

LRESULT FFFinder::MainDialog::onButtonSearchClicked( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled )
{
    if( !DoDataExchange( DDX_SAVE, IDC_EDIT_EXPRESSION ) )
        MessageBox( "Max search expression length is 260!", "Error", MB_OK | MB_ICONERROR );
    else if( strlen( m_strSearchExpression ) == 0 )
        MessageBox( "Search expression cannot be empty!", "Error", MB_OK | MB_ICONERROR );
    else if( m_strDirectoryRoot.empty() )
        MessageBox( "Root directory is undefined!", "Error", MB_OK | MB_ICONERROR );
    else
    {
        SearchResultDialog dlgSearchResultDialog( m_strDirectoryRoot, m_strSearchExpression );

        int nRet = dlgSearchResultDialog.DoModal( m_hWnd );
        if( nRet == 0 )
            ShowWindow( SW_SHOW );
        else
            EndDialog( 0 );
    }
    return 0;
}

LRESULT FFFinder::MainDialog::onEraseBkgnd( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    SmartDrawer drawer( reinterpret_cast<HDC>(wParam) );
    
    RECT rect = {};
    GetClientRect( &rect );

    COLORREF colorStart = RGB( 255, 255, 255 ), colorEnd = RGB( 201, 223, 223 );
    int left = 0, top = 0, right = rect.right, bottom = 23;

    drawer.fillGradientRect( { 0, 0 }, { rect.right, 23 }, colorStart, colorEnd, GRADIENT_FILL_RECT_V );
    drawer.fillGradientRect( { 0, 23 }, { rect.right, 46 }, colorEnd, colorStart, GRADIENT_FILL_RECT_V );
    colorEnd = RGB( 210, 211, 210 );
    drawer.fillGradientRect( { 0, 46 }, { rect.right, rect.bottom }, colorStart, colorEnd, GRADIENT_FILL_RECT_V );

    return LRESULT();
}
