#pragma once
#include <Windows.h>

namespace FFFinder
{
    class SmartDrawer
    {
    public:
        SmartDrawer( HDC hDc );

        bool fillGradientRect( POINT pointLT,
                       POINT pointRB,
                       COLORREF colorStart,
                       COLORREF colorEnd,
                       bool isVertical = true );
        bool fillGradientTriangle( POINT point1,
                           POINT point2,
                           POINT point3,
                           COLORREF color1,
                           COLORREF color2,
                           COLORREF color3 );

        bool drawLine( int x1, int y1, int x2, int y2 );

        HDC getHDC();

    private:
        HDC m_hDc;

        void initVertext( TRIVERTEX& vertex, POINT point, COLORREF color );
    };
}
