#include "styled_button.h"
#include "resource.h"


FFFinder::StyledButton::StyledButton( HBITMAP hBitmapDefault, HBITMAP hBitmapPressed, HBITMAP hBitmapHovered ) :
    CWindowImpl(),
    m_hBitmapDefault(hBitmapDefault),
    m_hBitmapPressed( hBitmapPressed ),
    m_hBitmapHovered( hBitmapHovered )
{
    
}

FFFinder::StyledButton::~StyledButton()
{
    DeleteObject( m_hBitmapDefault );
    DeleteObject( m_hBitmapPressed );
    DeleteObject( m_hBitmapHovered );
}

void FFFinder::StyledButton::DrawItem( LPDRAWITEMSTRUCT lpDIS )
{
    HDC hLocalDC = CreateCompatibleDC( lpDIS->hDC );
    BITMAP bitmap = {};
    HBITMAP hOldBmp = NULL;

    if( lpDIS->itemState & ODS_SELECTED )
    {
        GetObject( m_hBitmapPressed, sizeof( BITMAP ), reinterpret_cast<LPSTR>(&bitmap) );
        hOldBmp = reinterpret_cast<HBITMAP>(SelectObject( hLocalDC, m_hBitmapPressed ));
    }
    else if( m_isCursorHovered )
    {
        GetObject( m_hBitmapHovered, sizeof( BITMAP ), reinterpret_cast<LPSTR>(&bitmap) );
        hOldBmp = reinterpret_cast<HBITMAP>(SelectObject( hLocalDC, m_hBitmapHovered ));
    }
    else
    {
        GetObject( m_hBitmapDefault, sizeof( BITMAP ), reinterpret_cast<LPSTR>(&bitmap) );
        hOldBmp = reinterpret_cast<HBITMAP>(SelectObject( hLocalDC, m_hBitmapDefault ));
    }

    StretchBlt( lpDIS->hDC,
                lpDIS->rcItem.left,
                lpDIS->rcItem.top,
                lpDIS->rcItem.right,
                lpDIS->rcItem.bottom,
                hLocalDC,
                0,
                0,
                bitmap.bmWidth,
                bitmap.bmHeight,
                SRCCOPY );

    SelectObject( hLocalDC, hOldBmp );
    DeleteDC( hLocalDC );
}

LRESULT FFFinder::StyledButton::onMouseMove( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    if( !m_isCursorHovered )
    {
        m_isCursorHovered = true;
        TRACKMOUSEEVENT tme;
        tme.cbSize = sizeof( tme );
        tme.hwndTrack = m_hWnd;
        tme.dwFlags = TME_LEAVE;
        TrackMouseEvent( &tme );
        
        Invalidate();
    }
    return 0;
}

LRESULT FFFinder::StyledButton::onMouseLeave( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    m_isCursorHovered = false;
    Invalidate();
    return LRESULT();
}
