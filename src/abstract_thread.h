#pragma once
#include <Windows.h>

namespace FFFinder
{
	class AbstractThread
	{
	public:
		AbstractThread() = default;
		virtual ~AbstractThread();

		void start();
		void stop();

	protected:
		DWORD m_idThread = 0;
		HANDLE m_hThread = NULL;
		HANDLE hTerminateEvent = NULL;

		static DWORD WINAPI threadProc( LPVOID param );

		virtual DWORD run() = 0;
	};
}
