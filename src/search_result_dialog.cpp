#include "search_result_dialog.h"
#include "smart_drawer.h"


FFFinder::SearchResultDialog::SearchResultDialog( const std::string& strDirectoryRoot, const std::string& strSearchExpression ) :
    m_butBack( LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_BACK ) ),
               LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_BACK_PRESSED ) ),
               LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_BACK_HOVER ) ) ),
    m_butCancel( LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_CANCEL ) ),
                 LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_CANCEL_PRESSED ) ),
                 LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_CANCEL_HOVER ) ) ),
    m_fileFolderFinder( m_lvSearchResult, strDirectoryRoot )
{
    m_fileFolderFinder.setSearchExpression( strSearchExpression );
}

void FFFinder::SearchResultDialog::initColumns()
{
    LPCSTR arrHeaderText[] = { "Name", "Size", "Type", "Date", "Folder" };
    const int arrWidthHeader[] = { 150, 69, 70, 80, 150 };

    LVCOLUMNA lvColumn = {};
    lvColumn.mask = LVCF_WIDTH | LVCF_TEXT;
    lvColumn.cx = 250;
    for( int i = 0; i < 5; ++i )
    {
        lvColumn.cx = arrWidthHeader[i];
        lvColumn.pszText = const_cast<LPSTR>(arrHeaderText[i]);
        if( m_lvSearchResult.InsertColumn( i, &lvColumn ) == -1 )
            break;
    }
}

LRESULT FFFinder::SearchResultDialog::onInitDialog( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    m_lvSearchResult.SubclassWindow( GetDlgItem( IDC_LIST_SEARCH_RESULT ) );
    m_lvSearchResult.SetExtendedListViewStyle( LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER );

    m_butBack.SubclassWindow( GetDlgItem( IDC_BUTTON_BACK ) );
    m_butCancel.SubclassWindow( GetDlgItem( IDC_BUTTON_CANCEL ) );

    m_hBitmapSkin = LoadBitmap( GetModuleHandle( 0 ), MAKEINTRESOURCE( IDB_SEARCH_DIALOG_SKIN ) );

    initColumns();

    m_fileFolderFinder.start();

    return LRESULT();
}

LRESULT FFFinder::SearchResultDialog::onClose( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    SetCursor( LoadCursor( NULL, IDC_ARROW ) );
    ShowCursor( true );
    EndDialog( !0 );
    return LRESULT();
}

LRESULT FFFinder::SearchResultDialog::onButtonBackClicked( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled )
{
    m_fileFolderFinder.stop();
    SetCursor( LoadCursor( NULL, IDC_ARROW ) );
    ShowCursor( true );

    EndDialog( 0 );
    return 0;
}


LRESULT FFFinder::SearchResultDialog::onButtonCancelClicked( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled )
{
    m_fileFolderFinder.stop();
    SetCursor( LoadCursor( NULL, IDC_ARROW ) );
    ShowCursor( true );

    return 0;
}

LRESULT FFFinder::SearchResultDialog::onEraseBkgnd( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    HDC hDC = reinterpret_cast<HDC>(wParam);
    RECT rect = {};
    GetClientRect( &rect );

    HDC hLocalDC = CreateCompatibleDC( hDC );
    BITMAP bitmap;
    GetObject( m_hBitmapSkin, sizeof( BITMAP ), reinterpret_cast<LPSTR>(&bitmap) );
    HBITMAP hOldBmp = reinterpret_cast<HBITMAP>(SelectObject( hLocalDC, m_hBitmapSkin ));

    StretchBlt( hDC, rect.left, rect.top, rect.right, rect.bottom, hLocalDC, 0, 0, bitmap.bmWidth, bitmap.bmHeight, SRCCOPY );

    SelectObject( hLocalDC, hOldBmp );
    DeleteDC( hLocalDC );
    return LRESULT();
}
